// JavaScript Document
//Caleb's Cryptographically Insecure Password Generator
//3-9-18

function generatePassword(){
	
	let length = Math.abs(Math.round(Number(document.getElementById("lengthBox").value)));
	let userPass = '';
	let currentIndex = 0;
		
	let randomArray = new Uint8Array(length);
	window.crypto.getRandomValues(randomArray);
	let set = generateSet().split("");
	
	if (set.length <= 0) {
		alert("Select at least one set");
		return;
	}
	
	if (length > 64)
		length = 64;
	
	//make a password
	
	for (i1 = 0; i1 < length; i1++){
		randomArray[i1] %= set.length;
	}
	
	for (i2 = 0; i2 < length; i2++){
		if ((currentIndex + randomArray[i2]) > (set.length - 1)){
			userPass += set[(currentIndex + randomArray[i2]) - set.length];
		} else {
			userPass += set[currentIndex + randomArray[i2]];
		}
	}
	
	document.getElementById("output").value = userPass;
	
}

function generateSet(){
	let userSet = '';
	
	if(document.getElementById("Lowercase").checked){
		userSet += 'qazwsxedcrfvtgbyhnujmikolp';
	}
	
	if(document.getElementById("Uppercase").checked){
		userSet += 'QAZWSXEDCRFVTGBYHNUJMIKOLP';
	}
	
	if(document.getElementById("Numbers").checked){
		userSet += '0123456789';
	}
	
	if(document.getElementById("Specials").checked){
		userSet += '!@#$%^&*-_=+|\/;:"\'';
	}
	
	if(document.getElementById("Brackets").checked){
		userSet += '<>[]{}()';
	}
	
	return userSet;
}

function clipboardCopy(){
	let copyText = document.getElementById("output");
	copyText.select();
	document.execCommand("Copy");
	//alert('Copied: ' + copyText.value + ' to the clipboard');
}